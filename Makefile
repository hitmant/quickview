# QuickView Makefile
SHELL      := /bin/bash
CXX        := g++
CXXFLAGS   := -Wall $(shell sdl2-config --cflags) -std=c++20
CXXLIBS    := $(shell sdl2-config --libs) -lSDL2_image -lSDL2_ttf -lm
INSTALLLOC := /usr/bin/

SRCDIR := src
OBJDIR := obj

vpath %.cc $(SRCDIR)
vpath %.hh $(SRCDIR)

TARGET := quickview
SRCS_CC:= main.cc types.cc ktx.cc
SRCS   := $(SRCS_CC)
OBJS   := $(addprefix $(OBJDIR)/, $(SRCS:.cc=.o))

ifeq ($(MAKECMDGOALS), dev)
CXXFLAGS += -Og -g
else
CXXFLAGS += -O3
endif

FONT := $(shell ls /usr/share/fonts/TTF/ |grep AnonymousPro-Regular.ttf)
ifeq ($(FONT), AnonymousPro-Regular.ttf)
CXXFLAGS += -D FONTNAME='"/usr/share/fonts/TTF/AnonymousPro-Regular.ttf"'
else
CXXFLAGS += -D FONTNAME='"fonts/AnonymousPro-Regular.ttf"'
$(warning WARNING!)
$(warning Font AnonymousPro-Regular.ttf was not found in /usr/share/fonts/)
$(warning The resulting binary [$(TARGET)] will not be portable and depends)
$(warning on the local font file in ./fonts/. To achieve portability)
$(warning install AnonymousPro-Regular.ttf in /usr/share/fonts/)
$(warning (installation via your package manager is recommended))
$(warning and re-run make to recompile $(TARGET))
endif

$(TARGET) : $(SRCS) $(OBJS)
	$(CXX) $(CXXFLAGS) $(CXXLIBS) -o $@ $(filter %.o, $^)

$(OBJDIR)/main.o : types.hh ktx.hh
$(OBJDIR)/types.o : types.hh
$(OBJDIR)/ktx.o : types.hh ktx.hh

$(OBJDIR)/%.o : %.cc | $(OBJDIR)
	$(CXX) $(CXXFLAGS) -c -o $@ $<

$(OBJDIR) :
	mkdir -p $(OBJDIR)

.PHONY: dev
dev : $(TARGET)

.PHONY: install
install : $(TARGET)
	mv $(TARGET) $(INSTALLLOC)

.PHONY: uninstall
uninstall :
	rm $(INSTALLLOC)/$(TARGET)


.PHONY: clean
clean :
	rm -drf $(OBJDIR)
	rm -f $(TARGET)
