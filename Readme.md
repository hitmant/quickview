# QuickView - Simple image viewer

[![Version](https://img.shields.io/badge/Version-1.1.0-green.svg)](https://shields.io/)
[![Licence](https://img.shields.io/badge/Licence-GPLv3.0-blue.svg)](https://shields.io/)

QuickView is a C++ port of my older project 'lightview.'
QuickView is supposed to be a simple and lightweight image viewer, one that 
can be fired within a second to take a quick look at an image.

QuickView is built using SDL2 library as its only dependency.

## Features:
- Can show images.
- Can zoom image.
- Can pan image.
- Has indicator of zoom level.
### New in 1.1.0:
- Can view _(some)_ KTX files.

## Usage:
To use QuickView, start the program with the filename of the image you want 
to view as the only command argument, for example:
```
$ quickview cat.png
```
The image should adjust itself to the center and zoomed so that it fits the 
screen. If the image is very large or very small it may not be scaled to fit 
the screen, as default zooming caps at 50 % and at 200 %. To pan the image use 
either `hjkl` or arrow keys to move the image. The keybinds should work 
naturally for vim users. To reset the pan and zoom use `=` or `0`.

To _attempt_ to view a KTX file, use the flag `-k` before the filename:
```
$ quickview -k brick.ktx
```

## Dependencies:
This program is dependent on the following items:
- SDL 2 - multimedia library
- AnonymousPro-Regular - font used for text rendering

## Building:
This program is built using GNU Make 4.3, GCC 10.1.0 and SDL 2.0.12.
To build this program simply clone the repo and run 
```
make
``` 
The resulting executable should be ready to be used. If you don't have
`AnonymousPro-Regular.ttf` file in your `/usr/share/fonts/TTF/` make
will give you warnings and instead use the provided file for the same
font, thus making the executable non-installable.

## Installation:
To install this program after building run
```
make install
```
as root and the program will be installed in your `/usr/bin`.

## Licence:
QuickView
Copyright (C) 2020  hitmant

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
