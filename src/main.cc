#include <iostream>
#include <memory>
#include <cmath>
#include <cstdlib>
#include <algorithm>
#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>

#include "types.hh"
#include "ktx.hh"

void ser_error(std::string s, decltype(SDL_GetError) info = SDL_GetError) { 
	std::cerr << s << info() << "\n"; 
	std::exit(1); 
}

const std::string version{
	"Quickview 0.Devel\n"
	"Licence GPLv3: GNU GPL version 3 <https://gnu.org/licences/gpl.html>\n"
	"This is free software: you are free to change and redistribute it.\n"
	"There is NO WARRANTY, to the extent permitted by law.\n"};

int main(int argc, char** argv) {
	if (argc < 2){
		std::cerr << "Invalid number of arguments. Usage:\n"
		          << argv[0] << "[image]\n";
		return 1;
	}

	/* Skip option arguments. TODO: implement option handling. */
	++argv;
	bool ktxfile = false;
	for (int i = 1; i < argc; ++i, ++argv) {
		if (argv[0][0] != '-') break;
		else if (argv[0][1] == 'v') {
			std::cout << version;
			return 0;
		}
		else if (argv[0][1] == 'k') {
			ktxfile = true;
		}
	}
	
	if ( SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS) ) 
		  ser_error("Failed to init SDL: ");
	atexit(SDL_Quit);

	if (!ktxfile) {
		if ( !IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG) ) 
			ser_error("IMG_Init failed: ", IMG_GetError); 
		atexit(IMG_Quit);
	}

	if ( TTF_Init() ) 
		  ser_error("TTF_Init failed: ", TTF_GetError);
	atexit(TTF_Quit);

	SDL_DisplayMode initmode{};
	SDL_GetCurrentDisplayMode(0, &initmode);

	window win= SDL_CreateWindow( 
			"Quickview", 
			SDL_WINDOWPOS_UNDEFINED, 
			SDL_WINDOWPOS_UNDEFINED, 
			initmode.w, 
			initmode.h, 
			SDL_WINDOW_RESIZABLE );
	if (win.p == nullptr) 
		ser_error("SDL_CreateWindow failed: ");

	renderer ren= SDL_CreateRenderer(win.p, -1, 0);
	if (ren.p == nullptr) 
		ser_error("Renderer creation failed: ");

	[]() { // Get rid of startup bob -bug, but don't leave e hanging.
		SDL_Event e{};
		SDL_WaitEvent(&e);
	}();

	surface loadingSurface{};
	if (ktxfile) {
		loadingSurface = load_ktx(*argv);
	} else {
		loadingSurface = IMG_Load(*argv);
		if (loadingSurface.p == nullptr) 
			ser_error("Loading image failed: ", IMG_GetError);
	}

	texture picture = SDL_CreateTextureFromSurface(
			ren.p, 
			loadingSurface.p);
	rectangle src{0, 0, loadingSurface.p->w, loadingSurface.p->h};

	font text_font = TTF_OpenFont(FONTNAME, 20);
	if (text_font.p == nullptr) 
		ser_error("Failed to open text_font.p file: ", TTF_GetError);

	auto  fdiv  = [](int n, int d) { 
		return static_cast<float>(n)/static_cast<float>(d); 
	};
	const rectangle mode = [&]() { 
		rectangle temp{};
		SDL_GetWindowSize(win.p, &temp.w, &temp.h);
		return temp;
	}();
	// Note: scale is a float, not a lambda, 
	// as this lambda is called immediately.
	float scale = [&]() -> float {
		float sx = fdiv(mode.w, src.w);
		float sy = fdiv(mode.h, src.h);
		if   (sx < 1.0f && sy < 1.0f) scale = std::max(sx, sy);
		else                          scale = std::min(sx, sy);
		return std::clamp(scale, 0.5f, 2.0f);
	}();
	auto computeDst = [&](int sx = 0, int sy = 0) -> rectangle {
		const int moveStep = 50;
		int x = (mode.w - (src.w * scale)) / 2;
		int y = (mode.h - (src.h * scale)) / 2;
		x += (sx * moveStep) * scale;
		y += (sy * moveStep) * scale;
		int w = src.w * scale;
		int h = src.h * scale;
		return rectangle{x, y, w, h};
	};

	rectangle dst = computeDst();
	const float origScale{scale};
	const rectangle dstOrig{dst};
	const rectangle srcOrig{src};
	int stepsx = 0;
	int stepsy = 0;

	auto print_scale = [&] { 
		int temp = static_cast<int>(scale*100.0f); 
		return (std::to_string(temp) + "%");  
	};

	SDL_Color text_color{255, 255, 255, 255};
	surface zoom_text_surface = TTF_RenderText_Solid(
			text_font.p, 
			print_scale().c_str(), 
			text_color);
	if (zoom_text_surface.p == nullptr) 
		ser_error("Failed to render zoom text: ", TTF_GetError);

	texture zoom_text_texture = SDL_CreateTextureFromSurface(
			ren.p, 
			zoom_text_surface.p);
	rectangle zoom_text_rect{
		mode.w - 100, 
		mode.h - 40, 
		zoom_text_surface.p->w, 
		zoom_text_surface.p->h};
	rectangle zoom_box_rect {
		mode.w - 105, 
		mode.h - 45, 
		zoom_text_surface.p->w + 10, 
		zoom_text_surface.p->h + 10};

	do {
		SDL_SetRenderDrawColor(ren.p, 0, 0, 0, 255);
		SDL_RenderClear(ren.p);
		SDL_SetRenderDrawColor(ren.p, 80, 80, 80, 255);
		SDL_RenderFillRect(ren.p, &zoom_box_rect);
		SDL_RenderCopy(ren.p, picture.p, &src, &dst);
		SDL_RenderCopy(ren.p, zoom_text_texture.p, NULL, &zoom_text_rect);
		SDL_RenderPresent(ren.p);

		SDL_Event event{};
			SDL_WaitEvent(&event);
		if (event.type == SDL_KEYUP){
			switch (event.key.keysym.sym){
			case SDLK_q:
				goto exit;
			case SDLK_PLUS:
				scale *= 1.1f;
				break;
			case SDLK_MINUS:
				scale *= 0.9f;
				break;
			case SDLK_EQUALS:
			case SDLK_0:
				scale = origScale;
				stepsy = 0;
				stepsx = 0;
				break;
			case SDLK_RIGHT:
			case SDLK_l:
				--stepsx;
				break;
			case SDLK_LEFT:
			case SDLK_h:
				++stepsx;
				break;
			case SDLK_DOWN:
			case SDLK_j:
				--stepsy;
				break;
			case SDLK_UP:
			case SDLK_k:
				++stepsy;
				break;
			}
			
			scale = std::clamp(scale, 0.2f, 5.0f);
			dst = computeDst(stepsx, stepsy);

			zoom_text_surface.p = TTF_RenderText_Solid(
					text_font.p, 
					print_scale().c_str(), 
					text_color);
			zoom_text_texture.p = SDL_CreateTextureFromSurface(
					ren.p, 
					zoom_text_surface.p);

			resize(zoom_text_rect, 
					zoom_text_surface.p->w, 
					zoom_text_surface.p->h);

			resize(zoom_box_rect, 
					zoom_text_surface.p->w + 10, 
					zoom_text_surface.p->h + 10);

		}
	} while (true);
	
exit:
	return 0;
}
