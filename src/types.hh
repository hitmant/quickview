#ifndef TYPES_HH
#define TYPES_HH
#include <SDL2/SDL_rect.h>
#include <SDL2/SDL_video.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_ttf.h>

struct rectangle : SDL_Rect {
	rectangle() = default;
	rectangle(int ix, int iy, int iw, int ih) {x = ix; y = iy; w = iw; h = ih;};
};

void resize(rectangle&, const int, const int);

struct window {
	SDL_Window* p = nullptr;

	window() = default;
	window(SDL_Window* w) { 
		if (p == nullptr)
			SDL_DestroyWindow(p);
		p = w;
	}
};

struct renderer {
	SDL_Renderer* p = nullptr;

	renderer() = default;
	renderer(SDL_Renderer* r) {
		if (p != nullptr) 
			SDL_DestroyRenderer(p);
		p = r;
	}
};

struct surface {
	SDL_Surface* p = nullptr;

	surface() = default;
	surface(SDL_Surface* s) {
		if (p != nullptr)
			SDL_FreeSurface(p);
		p = s;
	}
};

struct texture {
	SDL_Texture* p = nullptr;

	texture() = default;
	texture(SDL_Texture* t) {
		if (p != nullptr)
			SDL_DestroyTexture(p);
		p = t;
	}
};

struct font {
	TTF_Font* p = nullptr;

	font() = default;
	font(TTF_Font* f) {
		if (p != nullptr)
			TTF_CloseFont(p);
		p = f;
	}
};
#endif //TYPES_HH
