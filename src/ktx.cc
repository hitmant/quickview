#include <cstdio>
#include <cassert>
#include "ktx.hh"

#define GL_BYTE                                 0x1400
#define GL_UNSIGNED_BYTE                        0x1401
#define GL_SHORT                                0x1402
#define GL_UNSIGNED_SHORT                       0x1403
#define GL_INT                                  0x1404
#define GL_UNSIGNED_INT                         0x1405
#define GL_FLOAT                                0x1406
#define GL_2_BYTES                              0x1407
#define GL_3_BYTES                              0x1408
#define GL_4_BYTES                              0x1409
#define GL_DOUBLE                               0x140A

SDL_Surface* load_ktx(const char* filename) {
	FILE* file = fopen(filename, "rb");
	SDL_Surface* result = nullptr;


	uint8_t  identifier[12];
	uint8_t correct[12] = {0xAB, 0x4B, 0x54, 0x58, 0x20, 0x31,
						   0x31, 0xBB, 0x0D, 0x0A, 0x1A, 0x0A };
	uint32_t endianness;
	uint32_t gl_type;
	uint32_t gl_type_size;
	uint32_t gl_format;
	uint32_t gl_internal_format;
	uint32_t gl_base_int_format;
	uint32_t pixel_width;
	uint32_t pixel_height;
	uint32_t pixel_depth;
	uint32_t num_array_elems;
	uint32_t num_faces;
	uint32_t num_mipmaps;
	uint32_t keyval_byes;

	fread(identifier, 1, 12, file);
	fread(&endianness, 1, 4, file);
	fread(&gl_type, 1, 4, file);
	fread(&gl_type_size, 1, 4, file);
	fread(&gl_format, 1, 4, file);
	fread(&gl_internal_format, 1, 4, file);
	fread(&gl_base_int_format, 1, 4, file);
	fread(&pixel_width, 1, 4, file);
	fread(&pixel_height, 1, 4, file);
	fread(&pixel_depth, 1, 4, file);
	fread(&num_array_elems, 1, 4, file);
	fread(&num_faces, 1, 4, file);
	fread(&num_mipmaps, 1, 4, file);
	fread(&keyval_byes, 1, 4, file);

	for (int i = 0; i < 12; ++i)
		if (identifier[i] != correct[i])
			exit(2);
	assert(endianness == 0x04030201);
	//the file is a KTX file and ok to proceed.

	size_t data_size;
	uint8_t* raw_data = nullptr;
	uint8_t* pixel_data = nullptr;
	if (gl_type == GL_UNSIGNED_BYTE) {
		if (gl_format == 0x80e0 || gl_format == 0x80e1) {
			auto bpp = gl_format == 0x80e1 ? 4 : 3; //bytes per pixel
			data_size = pixel_width * pixel_height * bpp;
			raw_data = new uint8_t[data_size];

			result = SDL_CreateRGBSurfaceWithFormat(
					0,
					pixel_width,
					pixel_height,
					24,
					SDL_PIXELFORMAT_BGR888 );

			fread(raw_data, 1, data_size, file);
			pixel_data = new uint8_t[pixel_width*pixel_height*4];
			for (size_t i = 0; i < data_size/bpp; ++i) {
				uint8_t* current = &raw_data[i*bpp];
				uint8_t* pixel   = &pixel_data[i*4];
				pixel[0] = current[2];
				pixel[1] = current[1];
				pixel[2] = current[0];
				pixel[3] = gl_format == 0x80e1 ? raw_data[3] : 0;
			}
		}
		else if (gl_format == 0x1907) {
			data_size = pixel_width * pixel_height * 3;
			raw_data = new uint8_t[data_size];
			result = SDL_CreateRGBSurfaceWithFormat(
					0,
					pixel_width,
					pixel_height,
					24,
					SDL_PIXELFORMAT_RGB888 );

			fread(raw_data, 1, data_size, file);
			pixel_data = new uint8_t[pixel_width*pixel_height*4];
			for (size_t i = 0; i < data_size/3; ++i) {
				uint8_t* current = &raw_data[i*3];
				uint8_t* pixel   = &pixel_data[i*4];
				pixel[0] = current[0];
				pixel[1] = current[1];
				pixel[2] = current[2];
				pixel[4] = 0;
			}
		} else if (gl_format == 0x1903) {
			data_size = pixel_width * pixel_height;
			raw_data = new uint8_t[data_size];
			result = SDL_CreateRGBSurfaceWithFormat(
					0,
					pixel_width,
					pixel_height,
					32,
					SDL_PIXELFORMAT_RGBA8888 );
			fread(raw_data, 1, data_size, file);
			pixel_data = new uint8_t[data_size*4];
			for (size_t i = 0; i < data_size; ++i) {
				pixel_data[i*4] = 0x00000000;
				pixel_data[i*4] = raw_data[i];
			}
		}else {
			fprintf(stderr, "Unimplemented GL format: %x\n", gl_format);
			exit(3);
		}
	} else {
		fprintf(stderr, "Unimplemented GL type: %x\n", gl_type);
		exit(3);
	}
	result->pixels = pixel_data;
	delete [] raw_data;

	return result;
}
